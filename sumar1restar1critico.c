#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <math.h>
#define limite 1000000000
static int *variable_compartida;
static int *flag1;


int main(void)
{
    pid_t mas;
    pid_t menos;

    variable_compartida = mmap(NULL, sizeof *variable_compartida, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    flag1 = mmap(NULL, sizeof *variable_compartida, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
   *flag1 = 1;

    *variable_compartida = 0;

    if ((mas = fork()) < 0)
    {
        perror("algo salio mal \n");
    }

    if (mas > 0) //padre
    {
        //creando el segundo hijo
        if ((menos = fork()) < 0)
        {
            perror("algo salio mal \n");
        }
        if (menos == 0)
        {
            printf("soy el segundo hijo con pid %d y mi padre es %d  \n", getpid(), getppid());

            for (int i = 0; i < limite; i++)
            {
            while(*flag1){}
            *variable_compartida = *variable_compartida - 1;;
            *flag1 = 1;
        
            }
        }
        else if (menos > 0)
        {
            while (wait(NULL) > 0)
                ;
            printf("Soy el padre con pid %d \n", getpid());
            printf("La varible ahora tiene como resultado %d \n", *variable_compartida);
        }
    }
    else if (mas == 0) //hijo
    {
        printf("soy el primer hijo con pid %d y mi padre es %d \n", getpid(), getppid());


        for (int j = 0; j < limite; j++)
        {
            *variable_compartida = *variable_compartida +1;
            *flag1 = 0;
            while(*flag1 == 0){}
        }
    }
    return 0;
}